#Imagen base node
FROM node:8.15.0-alpine

#Directorio app y seteo user node
RUN mkdir /home/node/app && chown node:node -R /home/node/app
USER node

#Copia app node 
COPY . /home/node/app/
WORKDIR /home/node/app
RUN npm install && npm cache clean --force
RUN npm run test
EXPOSE 3000
CMD ["node", "index.js"]