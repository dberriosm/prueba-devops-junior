## Etapa 1 Construcción de CI/CD
![cicd](./img/cicd.jpeg)

La automatización es una de nuestras principales pasiones y utilizar las herramientas de **CI/CD** es parte de nuestro día a día. Esta etapa consiste en lo siguiente:

1. Crea un flujo exitoso para la aplicación utilizando la herramienta que mejor domines o consideres.
3. **BONUS**: Utilizar Gitlab-CI te dará puntos extra, ya que es nuestra herramienta de trabajo actual, adicionalmente si pudieras generar la prueba en una rama especifica del mismo proyecto, documentando las configuraciones necesarias para ejecutar este pipeline en un ambiente bien sea onprem o cloud. te dara puntos extras!!.

**RESPUESTA:**
Se genera fork de repositorio para trabajar en una rama especifica. 
Se implementa archivo .gitlab-ci.yml en la raiz del repositorio que permite ejecutar pipeline mediante Gitlab-CI. El pipeline implementa el build y deploy de la aplicacion NodeJs de forma local.

## Etapa 2 Conceptos basicos, experiencias previas

La automatización es una de nuestras principales pasiones y utilizar las herramientas de **CI/CD** es parte de nuestro día a día. Esta etapa consiste en lo siguiente:

1. ¿En que areas de una empresa puede ser util un perfil DEVOPS?

- Respuesta: El rol o perfil DEVOPS es util en el area TI de una empresa. El rol DevOps tiene gran importancia hoy en dia ya que genera colaboracion entre las areas de desarrollo y operaciones. Genera gran valor al apoyar en la automatizacion del despliegue de artefactos y aplicativos mejorando mucho la agilidad.

2. ¿Dentro de los stage de un pipeline cual seria el orden correcto para estructura de los siguientes: build, deploy, package_publish, unit_test, rollback?

- Respuesta: build, unit_test, package_publish, deploy, rollback.

3. A nivel de Kubernetes como seria paso a paso la manera correcta de: 1) generar un namespace para un nuevo aplicativo 2) conectarte a dicho kubernetes.

- Respuesta:  
Para el namespace: ***kubectl create namespace "nombrenamespace"***
Para conectarse a cluster Kubernetes variara dependiendo donde este alojado(Cloud - OnPremise)
Si fuese entorno cloud, se requiere una cuenta de servicio con acceso al proyecto/consola del cluster.En caso OnPremise, se debe tener acceso a la red interna. Para ambos casos se requiere un archivo kubeconfig con los permisos necesarios sobre el cluster o namepesaces.

4. **BONUS**: Dockerize la aplicación
![docker](./img/docker.jpeg)

¿Qué pasa con los contenedores? En este momento ya año 2021, los contenedores son un estándar en las implementaciones de aplicaciones **(en la nube o en sistemas locales)**. Entonces, el reto es:
1. Construir la imagen más pequeña que pueda. Escribe un buen Dockerfile :)
2. Ejecutar la app como un usuario diferente de root.

**RESPUESTA:**
Se genera archivo Dockerfile en la raiz del repositorio para dockerizar la aplicacion NodeJs. Se construye una imagen pequeña a partir de node-alpine y se ejecuta la aplicacion con usuario llamado ***node***

1) Clonar repositorio.
2) Se puede armar imagen y ejecutar contenedor de forma local:
Ejecuta el siguiente comando para armar la imagen: ***docker build -t reto_tottus:latest***
Ejecuta el siguiente comando para levantar contenedor: ***docker run -d --name nodejspru -p 3000:3000 reto_tottus:latest***
3) Se puede armar y ejecutar contenedor mediante pipeline automatizado por GitLab-CI pero como requisito previo se debe instalar y configurar Gitlab Runner.


¡Porque cada dia cuenta para crecer, exitos!
